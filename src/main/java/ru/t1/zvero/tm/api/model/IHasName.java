package ru.t1.zvero.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}