package ru.t1.zvero.tm.api.controller;

public interface ICommandController {

    void showExit();

    void showErrorArgument();

    void showErrorCommand();

    void showAbout();

    void showVersion();

    void showHelp();

    void showSystemInfo();

}